---
layout: blog_post
type: mac0214
title: Preparação de materiais
<!-- image: http://placehold.it/900x300 -->
---

O Hardware Livre USP tinha uma fonte que estava sem uso, e ela
aparentou ser útil para o projeto. Um problema dela era o fato
de ela fornecer apenas saídas com tensão de 12V, e os motores
que temos precisam de 24V para funcionar.

Um dos membros do grupo, Gabriel Capella, forneceu para o
projeto um transformador, que consegue transformar a saída de
12V em uma saída de 24V.

No mês de setembro, montei o tranformador na saída da fonte, e
coloquei um fio de tomada nela.

Hoje (15/10/2018), em minha casa, peguei os fios que
originalmente estavam ligados nos motores, e soldei em suas
pontas conectores fêmea, para que pudessem ser utilizados no
Shield CNC.
