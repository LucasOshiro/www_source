---
layout: blog_post
type: mac0214
title: Confecção do Poster
<!-- image: http://placehold.it/900x300 -->
---

Durante a quinta-feira, 8 de novembro, e na manhã sexta-feira, 9 de
novembro, trabalhei na confecção do poster. Na segunda-feira, 12
de novembro, imprimir ele e o fixei no bloco B do IME.
(é uma pena ele não mostra os últimos avanços da semana do break...).

O poster pode ser visto a seguir:

<embed src="{{site.url}}/{{site.pdf_dir}}/mac0214/poster.pdf" type="application/pdf"
       style="width: 100%; height: 800px">

