---
layout: blog_post
type: mac0214
title: Definindo um padrão para os protocolos
<!-- image: http://placehold.it/900x300 -->
---

No começo do projeto, eu e outro membro do grupo (Arthur)
decidimos que não iríamos usar um protocolo ASCII, e que o
programa que é executado no computador só iria enviar para a
impressora braille apenas 2 bytes, sendo que os 4 primeiros bits
seriam números de 0 a 16 identificando o comando, e os 12 bytes
restantes seriam usados para argumentos para o comando enviado.

Na teoria, isso é bem simples... Na prática, é necessário saber
mais a fundo o funcionamento do hardware: tanto os processadores
x86 quanto os microcontroladores ATMEGA do Arduino são
little-endian. Isso significa que os bytes menos significativos,
que representam os menores valores vêm à esquerda na memória,
enquanto que os bytes com maiores valores vêm à direita.

Comecei a fazer um
<a href="https://github.com/HardwareLivreUSP/braille/blob/master/
  parser simples </a> para ler os bytes enviados pelo computador
e interpretá-los como comandos para a impressora braille, bem
como uma
<a href="https://github.com/HardwareLivreUSP/braille/blob/master/
  classe Python </a> para abstrair a comunicação usando esse
  protocolo simples.

Apesar de isso já ter sido explicado em algumas disciplinas,
nunca tinha visto na pratica o <i> endianess </i>. Isso foi um
sério problema, porque demorei <b> muito </b> tempo para
entender por que o protocolo não estava funcionando! Depois de
funcionando e com tudo ligado nas partes montadas, já deu pra
ter uma ideia de como ficará o eixo de codificação:

<figure>
<video class="img-fluid rounded" controls>
  <source src="{{site.url}}/{{site.videos_dir}}/mac0214/2.mp4" type="video/mp4">
</video>
<figcaption> Funcionamento do eixo inferior </figcaption>
<figure>
