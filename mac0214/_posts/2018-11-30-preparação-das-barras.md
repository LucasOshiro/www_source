---
layout: blog_post
type: mac0214
title: Preparação das barras
<!-- image: http://placehold.it/900x300 -->
---


Na última sexta-feira (23/11/2018), imprimi as peças para a fixação das barras
roscadas e dos eixos. Elas precisaram ainda de mais alguns ajustes finos. Uma
tentativa de impressão foi feita no dia anterior, mas por problemas com o
carretel, o filamento quebrou e as peças não puderam ser concluídas.


Apesar disso, elas estão com uma aparente fragilidade, e
precisarei imprimir outras, talvez com outro filamento.


