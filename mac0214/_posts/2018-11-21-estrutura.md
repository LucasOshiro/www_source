---
layout: blog_post
type: mac0214
title: Montagem da estrutura
image: 10.jpg
---

Nesta semana de break, eu finalmente conseguir montar a
estrututa da impressora braille (!).

Para montar, utilizei as ferramentas que tinha em casa. O MDF
foi preso utilizando cola branca e pregos (o que não foi uma
escolha muito inteligente, que acabou trincando... mas era o que
tinha para o momento).

<figure>
  <img class="img-fluid rounded" src="{{site.url}}/{{site.images_dir}}/mac0214/10.jpg" alt="">
  <figcaption> Estrutura de MDF </figcaption>
</figure>
