jogo = {
    board: [[null, null, null, null],
            [null, null, null, null],
            [null, null, null, null],
            [null, null, null, null]],

    posNull: {
        i: 0,
        j: 0
    },

    blockElements: [[null, null, null, null],
                    [null, null, null, null],
                    [null, null, null, null],
                    [null, null, null, null]],

    inicia: function () {
        var boardElement = document.getElementById ("board");
        boardElement.innerHTML = "";
        
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 4; j++) {
                var val = i * 4 + j;
                this.board[i][j] = val;

                if (val == 15) {
                    this.board[i][j] = null;
                    this.posNull.i = i;
                    this.posNull.j = j;
                    this.blockElements[i][j] = null;
                    continue;
                }
                
                var blockElement = document.createElement ("div");
                blockElement.innerHTML = this.board[i][j];
                blockElement.classList.add ("slideBlock");
                blockElement.style.top = (i * 20) + "vmin";
                blockElement.style.left = (j * 20) + "vmin";
                this.blockElements[i][j] = blockElement;
                board.appendChild (blockElement);
            }
        }

        this.shuffle (200);
    },

    shuffle: function (n) {
        var vects = [
            {i: -1, j: 0},
            {i: 1, j: 0},
            {i: 0, j: -1},
            {i: 0, j: 1},
        ]

        for (var i = 0; i < n; i++) {
            var x = Math.floor (Math.random () * 4);
            this.move (vects[x]);
        }
    },

    move: function (vect) {
        var target = {
            i: jogo.posNull.i + vect.i,
            j: jogo.posNull.j + vect.j
        }

        if (target.i < 0 || target.i >= 4 ||
            target.j < 0 || target.j >= 4)
            return;

        this.board[this.posNull.i][this.posNull.j] = this.board[target.i][target.j];
        this.board[target.i][target.j] = null;


        var blockElement = this.blockElements[target.i][target.j];
        this.blockElements[target.i][target.j] = null;

        this.blockElements[this.posNull.i][this.posNull.j] = blockElement;
        blockElement.style.top = (parseInt (blockElement.style.top) - vect.i * 20) + "vmin";
        blockElement.style.left = (parseInt (blockElement.style.left) - vect.j * 20) + "vmin";

        this.posNull = target;
    },

    check: function () {
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 4; j++) {
                if (this.board[i][j] != null && this.board[i][j] != i * 4 + j)
                    return false;
            }
        }
        return true;
    }
}

document.body.onkeydown = function (event) {
    var vect = {
        i: 0,
        j: 0
    }
    
    switch (event.keyCode) {
    case 37:
        vect.j = 1;
        break;

    case 38:
        vect.i = 1;
        break;

    case 39:
        vect.j = -1;
        break;

    case 40:
        vect.i = -1;
        break;
    }

    jogo.move (vect);
    if (jogo.check ()) {
        alert ("!!!");
        jogo.inicia ();
    }
}

touchStart = null;
touchEnd = null;

document.body.ontouchstart = function (event) {
    touchStart = event.changedTouches.item (0);
}

document.body.ontouchend = function (event) {
    touchEnd = event.changedTouches.item (0);
    if (touchEnd == null || touchStart == null) return;

    var difX = touchEnd.screenX - touchStart.screenX;
    var difY = touchEnd.screenY - touchStart.screenY;

    var absX = Math.abs (difX);
    var absY = Math.abs (difY);

    var vect = {
        i: absY > absX ? - Math.round (difY / absY) : 0,
        j: absX >= absY ? - Math.round (difX / absX) : 0
    }

    jogo.move (vect);

    if (jogo.check ()) {
        alert ("!!!");
        jogo.inicia ();
    }
}

jogo.inicia ();
