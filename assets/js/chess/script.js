function simbolo (peca) {
    var carac = null;
    if (peca == null) return '';
    if (peca.cor == 'w') {
        carac = {
            'K': '&#9812;',
            'Q': '&#9813;',
            'R': '&#9814;',
            'B': '&#9815;',
            'N': '&#9816;',
            'P': '&#9817;'
        }
    }
    else if (peca.cor == 'b') {
        carac = {
            'K': '&#9818;',
            'Q': '&#9819;',
            'R': '&#9820;',
            'B': '&#9821;',
            'N': '&#9822;',
            'P': '&#9823;'
        }
    }
    else return '';
    return carac[peca.tipo];
}

function seleciona (casa) {
    casa.classList.add ('casaSelecionada');
    casaSelecionada = casa;
}

function deseleciona () {
    casaSelecionada.classList.remove ('casaSelecionada');
    casaSelecionada = null;
}

function marcaPossiveis (casas) {
    for (var i = 0; i < casas.length; i++) 
        celulas[casas[i][0]][casas[i][1]].classList.add ('casaPossivel');
    casasPossiveis = casas;
}

function desmarcaPossiveis () {
    for (var i = 0; i < casasPossiveis.length; i++) 
        celulas[casasPossiveis[i][0]][casasPossiveis[i][1]].classList.remove ('casaPossivel');
    casasPossiveis = [];
}

casaSelecionada = null;
casasPossiveis = [];
celulas = [[], [], [], [], [], [], [], []];

var Peca = {
    cor: null,
    tipo: null,
    moveu: false,
    linha: null,
    coluna: null,
    pulouPrimeira: false,
    ultimoMovimento: null,

    movimentosPossiveis: function () {
        var coef = this.cor == 'b' ? 1 : -1;
        var possiveis = [];
        
        switch (this.tipo) {
        case 'K':
            possiveis = [
                [this.linha - 1, this.coluna - 1],
                [this.linha - 1, this.coluna],  
                [this.linha - 1, this.coluna + 1],
                [this.linha, this.coluna - 1],  
                [this.linha, this.coluna + 1],  
                [this.linha + 1, this.coluna - 1],
                [this.linha + 1, this.coluna],  
                [this.linha + 1, this.coluna + 1]
            ];

            for (var i = 0; i < possiveis.length; i++)
                if (possiveis[i][0] >= 0 && possiveis[i][0] < 8 &&
                    possiveis[i][1] >= 0 && possiveis[i][1] < 8 &&
                    jogo.tabuleiro[possiveis[i][0]][possiveis[i][1]] != null &&
                    jogo.tabuleiro[possiveis[i][0]][possiveis[i][1]].cor == this.cor) {
                    possiveis.splice (i--, 1);
                }

            // Roque
            if (!this.moveu) {
                if (jogo.tabuleiro[this.linha][5] == null &&
                    jogo.tabuleiro[this.linha][6] == null &&
                    jogo.tabuleiro[this.linha][7].tipo == 'R' &&
                    !jogo.tabuleiro[this.linha][7].moveu)
                    possiveis.push([this.linha, 6])

                if (jogo.tabuleiro[this.linha][1] == null &&
                    jogo.tabuleiro[this.linha][2] == null &&
                    jogo.tabuleiro[this.linha][3] == null &&
                    !jogo.tabuleiro[this.linha][0].moveu)
                    possiveis.push([this.linha, 1])
            }
            
            break;

        case 'Q':
            for (var i = this.linha - 1; i >= 0; i--) {
                if (jogo.tabuleiro[i][this.coluna] != null) {
                    if (jogo.tabuleiro[i][this.coluna].cor != this.cor)
                        possiveis.push ([i, this.coluna]);
                    break;
                }

                possiveis.push ([i, this.coluna]);
            }

            for (var i = this.linha + 1; i < 8; i++) {
                if (jogo.tabuleiro[i][this.coluna] != null) {
                    if (jogo.tabuleiro[i][this.coluna].cor != this.cor)
                        possiveis.push ([i, this.coluna]);
                    break;
                }
                possiveis.push ([i, this.coluna]);
            }

            for (var i = this.coluna - 1; i >= 0; i--) {
                if (jogo.tabuleiro[this.linha][i] != null) {
                    if (jogo.tabuleiro[this.linha][i].cor != this.cor)
                        possiveis.push ([this.linha, i]);
                    break;
                }
                possiveis.push ([this.linha, i]);
            }

            for (var i = this.coluna + 1; i < 8; i++) {
                if (jogo.tabuleiro[this.linha][i] != null) {
                    if (jogo.tabuleiro[this.linha][i].cor != this.cor)
                        possiveis.push ([this.linha, i]);
                    break;
                }
                possiveis.push ([this.linha, i]);
            }

            for (var i = 1; this.linha - i >= 0 && this.coluna - i >= 0; i++) {
                if (jogo.tabuleiro[this.linha - i][this.coluna - i] != null) {
                    if (jogo.tabuleiro[this.linha - i][this.coluna - i].cor != this.cor)
                        possiveis.push([this.linha - i, this.coluna - i]);
                    break;
                }
                possiveis.push([this.linha -i, this.coluna - i]);
            }

            for (var i = 1; this.linha - i >= 0 && this.coluna + i < 8; i++) {
                if (jogo.tabuleiro[this.linha - i][this.coluna + i] != null) {
                    if (jogo.tabuleiro[this.linha - i][this.coluna + i].cor != this.cor)
                        possiveis.push([this.linha - i, this.coluna + i]);
                    break;
                }
                possiveis.push([this.linha -i, this.coluna + i]);
            }

            for (var i = 1; this.linha + i < 8 && this.coluna - i >= 0; i++) {
                if (jogo.tabuleiro[this.linha + i][this.coluna - i] != null) {
                    if (jogo.tabuleiro[this.linha + i][this.coluna - i].cor != this.cor)
                        possiveis.push([this.linha + i, this.coluna - i]);
                    break;
                }
                possiveis.push([this.linha + i, this.coluna - i]);
            }

            for (var i = 1; this.linha + i < 8 && this.coluna + i < 8; i++) {
                if (jogo.tabuleiro[this.linha + i][this.coluna + i] != null) {
                    if (jogo.tabuleiro[this.linha + i][this.coluna + i].cor != this.cor)
                        possiveis.push([this.linha + i, this.coluna + i]);
                    break;
                }
                possiveis.push([this.linha + i, this.coluna + i]);
            }
            break;

        case 'R':
            for (var i = this.linha - 1; i >= 0; i--) {
                if (jogo.tabuleiro[i][this.coluna] != null) {
                    if (jogo.tabuleiro[i][this.coluna].cor != this.cor)
                        possiveis.push ([i, this.coluna]);
                    break;
                }

                possiveis.push ([i, this.coluna]);
            }

            for (var i = this.linha + 1; i < 8; i++) {
                if (jogo.tabuleiro[i][this.coluna] != null) {
                    if (jogo.tabuleiro[i][this.coluna].cor != this.cor)
                        possiveis.push ([i, this.coluna]);
                    break;
                }
                possiveis.push ([i, this.coluna]);
            }

            for (var i = this.coluna - 1; i >= 0; i--) {
                if (jogo.tabuleiro[this.linha][i] != null) {
                    if (jogo.tabuleiro[this.linha][i].cor != this.cor)
                        possiveis.push ([this.linha, i]);
                    break;
                }
                possiveis.push ([this.linha, i]);
            }

            for (var i = this.coluna + 1; i < 8; i++) {
                if (jogo.tabuleiro[this.linha][i] != null) {
                    if (jogo.tabuleiro[this.linha][i].cor != this.cor)
                        possiveis.push ([this.linha, i]);
                    break;
                }
                possiveis.push ([this.linha, i]);
            }

            break;

        case 'B':
            for (var i = 1; this.linha - i >= 0 && this.coluna - i >= 0; i++) {
                if (jogo.tabuleiro[this.linha - i][this.coluna - i] != null) {
                    if (jogo.tabuleiro[this.linha - i][this.coluna - i].cor != this.cor)
                        possiveis.push([this.linha - i, this.coluna - i]);
                    break;
                }
                possiveis.push([this.linha -i, this.coluna - i]);
            }

            for (var i = 1; this.linha - i >= 0 && this.coluna + i < 8; i++) {
                if (jogo.tabuleiro[this.linha - i][this.coluna + i] != null) {
                    if (jogo.tabuleiro[this.linha - i][this.coluna + i].cor != this.cor)
                        possiveis.push([this.linha - i, this.coluna + i]);
                    break;
                }
                possiveis.push([this.linha -i, this.coluna + i]);
            }

            for (var i = 1; this.linha + i < 8 && this.coluna - i >= 0; i++) {
                if (jogo.tabuleiro[this.linha + i][this.coluna - i] != null) {
                    if (jogo.tabuleiro[this.linha + i][this.coluna - i].cor != this.cor)
                        possiveis.push([this.linha + i, this.coluna - i]);
                    break;
                }
                possiveis.push([this.linha + i, this.coluna - i]);
            }

            for (var i = 1; this.linha + i < 8 && this.coluna + i < 8; i++) {
                if (jogo.tabuleiro[this.linha + i][this.coluna + i] != null) {
                    if (jogo.tabuleiro[this.linha + i][this.coluna + i].cor != this.cor)
                        possiveis.push([this.linha + i, this.coluna + i]);
                    break;
                }
                possiveis.push([this.linha + i, this.coluna + i]);
            }

            break;

        case 'N':
            if (this.linha - 2 >= 0) {
                if (this.coluna - 1 >= 0)
                    possiveis.push ([this.linha - 2, this.coluna - 1]);
                if (this.coluna + 1 < 8)
                    possiveis.push ([this.linha - 2, this.coluna  + 1]);
            }

            if (this.linha - 1 >= 0) {
                if (this.coluna - 2 >= 0)
                    possiveis.push ([this.linha - 1, this.coluna - 2]);
                if (this.coluna + 2 < 8)
                    possiveis.push ([this.linha - 1, this.coluna  + 2]);
            }

            if (this.linha + 1 < 8) {
                if (this.coluna - 2 >= 0)
                    possiveis.push ([this.linha + 1, this.coluna - 2]);
                if (this.coluna + 2 < 8)
                    possiveis.push ([this.linha + 1, this.coluna + 2]);
            }

            if (this.linha + 2 < 8) {
                if (this.coluna - 1 >= 0)
                    possiveis.push ([this.linha + 2, this.coluna - 1]);
                if (this.coluna + 1 < 8)
                    possiveis.push ([this.linha + 2, this.coluna + 1]);
            }

            for (var i = 0; i < possiveis.length; i++)
                if (possiveis[i][0] >= 0 && possiveis[i][0] <8  &&
                    possiveis[i][1] >= 0 && possiveis[i][1] < 8 &&
                    jogo.tabuleiro[possiveis[i][0]][possiveis[i][1]] != null &&
                    jogo.tabuleiro[possiveis[i][0]][possiveis[i][1]].cor == this.cor) {
                    possiveis.splice (i--, 1);
                }
            
            break;
            
        case 'P':
            if (this.linha < 8 &&
                jogo.tabuleiro[this.linha + coef][this.coluna] == null) {
                possiveis.push ([this.linha + coef, this.coluna]);
                if (!this.moveu && jogo.tabuleiro[this.linha + 2 * coef][this.coluna] == null)
                    possiveis.push ([this.linha + 2 * coef, this.coluna]);
            }

            if (this.coluna >= 1 && this.linha < 8) 
                if (jogo.tabuleiro[this.linha + coef][this.coluna - 1] != null &&
                    jogo.tabuleiro[this.linha + coef][this.coluna - 1].cor != this.cor)
                    possiveis.push([this.linha + coef, this.coluna - 1]);

            if (this.coluna <= 7 && this.linha < 8 &&
                jogo.tabuleiro[this.linha + coef][this.coluna + 1] != null &&
                jogo.tabuleiro[this.linha + coef][this.coluna + 1].cor != this.cor)
                possiveis.push([this.linha + coef, this.coluna + 1]);


            // En Passant
            if (this.cor == 'w' && this.linha == 3 ||
                this.cor == 'b' && this.linha == 4) {
                if (this.coluna >= 1 &&
                    jogo.tabuleiro[this.linha][this.coluna - 1] != null &&
                    jogo.tabuleiro[this.linha][this.coluna - 1].tipo == 'P' &&
                    jogo.tabuleiro[this.linha][this.coluna - 1].ultimoMovimento == jogo.numeroMovimentos - 1 &&
                    jogo.tabuleiro[this.linha][this.coluna - 1].pulouPrimeira)
                    possiveis.push ([this.linha + coef, this.coluna - 1])

                else if (this.coluna < 8 &&
                         jogo.tabuleiro[this.linha][this.coluna + 1] != null &&
                         jogo.tabuleiro[this.linha][this.coluna + 1].tipo == 'P' &&
                         jogo.tabuleiro[this.linha][this.coluna + 1].ultimoMovimento == jogo.numeroMovimentos - 1 &&
                         jogo.tabuleiro[this.linha][this.coluna + 1].pulouPrimeira)
                    possiveis.push ([this.linha + coef, this.coluna + 1])

            }

            break;
        }

        for (var i = 0; i < possiveis.length; i++) {
            if (possiveis[i][0] < 0 || possiveis[i][0] >= 8 ||
                possiveis[i][1] < 0 || possiveis[i][1] >= 8) {
                possiveis.splice (i--, 1);
                continue;
            }
        }

        return possiveis;
    }
}

var jogo = {
    tabuleiro: null,
    casas: null,
    jogadorAtual: null,
    numeroMovimentos: null,
    pecasBrancas: null,
    pecasPretas: null,


    inicia: function () {
        var pecas = ['R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R'];
        this.tabuleiro = [];
        this.casas = [];
        this.pecasBrancas = [];
        this.pecasPretas = [];
        this.jogadorAtual = 'w';
        this.numeroMovimentos = 0;
        
        for (var i = 0; i < 8; i++) {
            this.tabuleiro[i] = [];
            for (var j = 0; j < 8; j++) {
                this.tabuleiro[i].push(null);
            }
        }

        for (var j = 0; j < 8; j++) {
            this.insere (0, j, 'b', pecas[j]);
            this.insere (1, j, 'b', 'P');
            this.insere (6, j, 'w', 'P');
            this.insere (7, j, 'w', pecas[j]);
        }

        var casas = document.getElementsByClassName('casa');

        for (var i = 0; i < casas.length; i++) {
            casas[i].onclick = function (event) {
                var linha = parseInt (event.target.getAttribute('linha'));
                var coluna = parseInt (event.target.getAttribute('coluna'));
                
                if (casaSelecionada != null) {
                    var igual = (casaSelecionada == event.target);
                    var linhaSelecionada = parseInt (casaSelecionada.getAttribute ('linha'));
                    var colunaSelecionada = parseInt (casaSelecionada.getAttribute ('coluna'));

                    deseleciona ();
                    desmarcaPossiveis ();
                    if (igual) return;

                    var peca = jogo.tabuleiro[linhaSelecionada][colunaSelecionada];
                    var p = peca.movimentosPossiveis ();

                    for (var j = 0; j < p.length; j++) {
                        if (p[j][0] == linha && p[j][1] == coluna) {
                            // En Passant
                            if (peca.tipo == 'P' && coluna != colunaSelecionada &&
                                jogo.tabuleiro[linha][coluna] == null) {
                                jogo.remove (linhaSelecionada, coluna);
                            }

                            if (jogo.tabuleiro[linha][coluna] != null)
                                jogo.remove (linha, coluna);
                            jogo.move (peca, linha, coluna);

                            // Roque
                            if (peca.tipo == 'K') {
                                switch (Math.abs (coluna - colunaSelecionada)) {
                                case 2:
                                    jogo.move (jogo.tabuleiro[linha][7], linha, 5);
                                    break;

                                case 3:
                                    jogo.move (jogo.tabuleiro[linha][0], linha, 2);
                                    break;
                                }
                            }

                            // Promocao
                            if (peca.tipo == 'P' && linha % 7 == 0) {
                                alert ("PROMOVE");
                            }

                            peca.ultimoMovimento = jogo.numeroMovimentos++;
                            jogo.verificaXeque ();
                            
                            jogo.jogadorAtual = jogo.jogadorAtual == 'w' ? 'b' : 'w';
                            jogo.mostra ();

                            return;
                        }
                    }
                }

                if (jogo.tabuleiro[linha][coluna] != null &&
                    jogo.tabuleiro[linha][coluna].cor == jogo.jogadorAtual) {
                    seleciona (event.target);
                    marcaPossiveis(jogo.tabuleiro[linha][coluna].movimentosPossiveis ());
                }
            }
        }

        this.mostra ();
    },

    mostra: function () {
        var casas = document.getElementsByClassName('casa');

        for (var i = 0; i < casas.length; i++) {
            var linha = casas[i].getAttribute ('linha');
            var coluna = casas[i].getAttribute ('coluna');
            
            casas[i].innerHTML = simbolo (this.tabuleiro[linha][coluna]);
            celulas[linha][coluna] = casas[i];
        }
    },

    move: function (peca, linhaNova, colunaNova) {
        linhaAntiga = peca.linha;
        colunaAntiga = peca.coluna;
        
        this.tabuleiro[linhaAntiga][colunaAntiga] = null;
        
        peca.linha = linhaNova;
        peca.coluna = colunaNova;
        
        if (peca.tipo == 'P' && !peca.moveu && Math.abs (linhaNova - linhaAntiga) == 2)
            peca.pulouPrimeira = true
        
        peca.moveu = true;
        this.tabuleiro[linhaNova][colunaNova] = peca;
    },

    insere: function (linha, coluna, cor, tipo) {
        var peca = Object.create (Peca);
        var pecas = cor == 'w' ? this.pecasBrancas : this.pecasPretas;
        peca.cor = cor;
        peca.tipo = tipo;
        peca.linha = linha;
        peca.coluna = coluna;
        
        pecas.push (peca);
        this.tabuleiro[linha][coluna] = peca;
        return peca;
    },

    remove: function (linha, coluna) {
        var peca = this.tabuleiro[linha][coluna];
        var pecas = peca.cor == 'w' ? this.pecasBrancas : this.pecasPretas;
        console.log ("here")
        pecas.splice(pecas.indexOf(peca), 1);
        this.tabuleiro[linha][coluna] = null;
    },

    verificaXeque: function () {
        var pecas = this.jogadorAtual == 'w' ? this.pecasBrancas : this.pecasPretas;
        var pecasOponente = this.jogadorAtual == 'b' ? this.pecasBrancas : this.pecasPretas;

        var rei = null;
        var xeque = false;

        for (var i = 0; i < pecasOponente.length; i++) 
            if (pecasOponente[i].tipo == 'K') {
                rei = pecasOponente[i];
                break;
            }

        console.log (rei);

        for (var i = 0; i < pecas.length; i++) {
            var movimentos = pecas[i].movimentosPossiveis ();
            for (var j = 0; j < movimentos.length; j++) {
                if (movimentos[j][0] == rei.linha &&
                    movimentos[j][1] == rei.coluna) {
                    xeque = true;
                }
            }
        }

        if (xeque) alert ("xeque");
    }
}

jogo.inicia();
