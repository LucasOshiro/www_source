Game = {
    initialize: function (h, w) {
        this.h = h;
        this.w = w;
        
        this.mineTotal = Math.floor (h * w / 6);
        var r = [];
        this.mines = [];
        this.board = [];
        this.opened = [];
        this.cellElements = [];
        this.openedCount = 0;

        for (var i = 0; i < h; i++) {
            var rowBoard = [];
            var rowOpened = [];
            var rowElements = [];
            for (var j = 0; j < w; j++) {
                rowBoard.push (0);
                rowOpened.push (false);
                rowElements.push (null);
            }
            this.board.push (rowBoard);
            this.opened.push (rowOpened);
            this.cellElements.push (rowElements);
        }
        
        for (var i = 0; i < this.mineTotal; i++) {
            var rand;
            while (r.indexOf (rand = Math.floor (Math.random () * h * w)) >= 0);
            r.push (rand);
            
            var mine = {
                row: Math.floor (rand / w),
                col: rand % w
            }

            this.mines.push (mine);
            this.board[mine.row][mine.col] = -this.mineTotal;

            if (mine.row > 0) this.board[mine.row - 1][mine.col]++;
            if (mine.col < w - 1) this.board[mine.row][mine.col + 1]++;
            if (mine.row < h - 1) this.board[mine.row + 1][mine.col]++;
            if (mine.col > 0) this.board[mine.row][mine.col - 1]++;

            if (mine.row > 0) {
                if (mine.col > 0) this.board[mine.row - 1][mine.col - 1]++;
                if (mine.col < w - 1) this.board[mine.row - 1][mine.col + 1]++;
            }
            if (mine.row < h - 1) {
                if (mine.col > 0) this.board[mine.row + 1][mine.col - 1]++;
                if (mine.col < w - 1) this.board[mine.row + 1][mine.col + 1]++;
            }
        }

        document.getElementById ("board").innerHTML = "";
        
        for (var i = 0; i < h; i++) {
            var row = document.createElement ("div");
            row.style.height = 1 / h * 100 + "%";
            for (var j = 0; j < w; j++) {
                var cell = document.createElement ("div");
                cell.classList.add ("cell");
                cell.setAttribute ("row", i);
                cell.setAttribute ("col", j);
                cell.style.width = 1 / w * 100 + "%";
                cell.onclick = cellClickEvent;

                this.cellElements[i][j] = cell;
                row.appendChild (cell);
            }
            document.getElementById ("board").appendChild (row);
        }
    },

    open: function (row, col) {
        if (row < 0 ||
            col < 0 ||
            row >= this.h ||
            col >= this.w ||
            this.opened[row][col]) return;

        this.opened[row][col] = true;
        this.openedCount++;

        var val = this.board[row][col];

        if (val < 0) {
            for (var i = 0; i < this.mineTotal; i++) {
                var mine = this.mines[i];
                this.cellElements[mine.row][mine.col].style.background = "red";
                var el = document.createElement ("div");
                el.setAttribute ("row", mine.row);
                el.setAttribute ("col", mine.col);
                el.innerHTML = "&#128163;";
                el.color = "black";
                this.cellElements[mine.row][mine.col].appendChild (el);
                this.opened[mine.row][mine.col] = true;
            }

            this.block ();
            window.setTimeout (function () {showEndMessage (false)}, 1000);
            return;
        }

        var el = document.createElement ("div");
        el.setAttribute ("row", row);
        el.setAttribute ("col", col);

        if (val == 0) {
            this.open (row - 1, col - 1);
            this.open (row - 1, col);
            this.open (row - 1, col + 1);
            this.open (row, col - 1);
            this.open (row, col + 1);
            this.open (row + 1, col - 1);
            this.open (row + 1, col);
            this.open (row + 1, col + 1);
        }
        else {
            el.innerHTML = val;
            el.style.color = "hsl(" + (120 - val * 15) + ", 80%, 50%)";
            console.log ("hsl(" + (1.875 * val * val - 30 * val + 120) + ", 100%, 50%)");
        }
        
        this.cellElements[row][col].appendChild (el);
        this.cellElements[row][col].style.background = "white";

        if (this.openedCount == this.h * this.w - this.mineTotal) {
            this.block ();
            setTimeout (function () {showEndMessage (true)}, 1000);
        }
    },

    block: function () {
        for (var i = 0; i < this.h; i++)
                for (var j = 0; j < this.w; j++)
                    this.cellElements[i][j].onclick = undefined;

    }
}
