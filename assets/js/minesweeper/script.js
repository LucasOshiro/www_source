function hideNewGameScreen () {
    document.getElementById ("newgame").style.display = "none";
}

function showNewGameScreen () {
    document.getElementById ("newgame").style.display = "block";
}

function cellClickEvent (event) {
    var row = parseInt(event.target.getAttribute ("row"));
    var col = parseInt(event.target.getAttribute ("col"));

    Game.open (row, col);
}

function hideEndMessage () {
    document.getElementById ("endMessage").style.display = "none";
}

function showEndMessage (win) {
    console.log ("here");
    document.getElementById ("endMessage").style.display = "block";
    document.getElementById ("endMessageTitle").innerHTML = win ? "Ganhou" : "Perdeu";
}

document.getElementById ("start_id").onclick = function () {
    var h = parseInt (document.getElementById ("height_id").value);
    var w = parseInt (document.getElementById ("width_id").value);

    if (h < 5 || w < 5 || h > 50 || w > 50) return;
    
    hideNewGameScreen ();
    Game.initialize (h, w);
}

document.getElementById ("playAgain_id").onclick = function () {
    hideEndMessage ();
    showNewGameScreen ();
}
