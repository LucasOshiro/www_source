---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home

cards:
  -
    title: MAC0499
    description: Blog do meu TCC
    link: mac0499

  -
    title: Impressora Braille
    description: Impressora braille (ainda em desenvolvimento) feito no grupo Hardware Livre
    link: mac0214

  -
    title: Musical LPT
    description: Programa feito em C para tocar música na porta paralela
    link: https://hardwarelivreusp.org/projetos/2019/03/02/musical_lpt/

  -
    title: PCDisplay
    description: Display no gabinete que exibe informações sobre o computador, feito com Arduino
    link: https://github.com/lucasoshiro/pcdisplay

  -
    title: Lambdasort
    description: Função em Python que ordena uma lista feita apenas com lambdas, a partir do Quicksort
    link: https://github.com/lucasoshiro/lambdasort

  -
    title: Sliding Puzzle
    description: Apenas um sliding puzzle feito para praticar js e css
    link: slidingpuzzle

  -
    title: Campo Minado
    description: Campo minado feito para praticar js e css
    link: minesweeper

  -
    title: Xadrez
    description: Xadrez feito para praticar js e css (incompleto)
    link: chess

  -
    title: Métodos Numéricos
    description: Índice pra organizar os estudos de uma disciplina
    link: metodosnumericos

homepage_links:
  -
    title: GitHub
    icon: github.png
    link: https://github.com/lucasoshiro

  -
    title: GitLab
    icon: gitlab.png
    link: https://gitlab.com/lucasoshiro

  -
    title: Hardware Livre USP
    icon: hl.png
    link: https://hardwarelivreusp.org/

  -
    title: FLUSP
    icon: flusp.png
    link: https://flusp.ime.usp.br/

---
