---
layout: blog_post
type: mac0499
title: Cronograma
---

Abril
- montar o ambiente de medição de consumo de energia;
- preparação da apresentação do artigo que fala dos avanços
  obtidos até o momento (A ser apresentado no Workshop de
  Trabalhos de Iniciação Científica e de Graduação do SBRC)
- participação do Workshop de Trabalhos de Iniciação Científica
  e de Graduação do SBRC

Maio
- realização de experimentos para medição do consumo de energia
  com as aplicações sintéticas (vcgenmod/iperf)
- realização de experimentos para análise de desempenho
  com aplicações reais (logstash)

Junho
- realização de experimentos para análise de desempenho com
  aplicações reais (Kafka, Spark e ElasticSearch)

Julho
- integração da unidade Raspberry Pi com todo o sistema de
  detecção de intrusão

Agosto
- realização de experimentos para análise de precisão na
  detecção de ataques

Setembro
- realização de experimentos para análise de precisão na
  detecção de ataques

Outubro
- escrita do TCC

Novembro
- preparação do poster para exibição e dos slides para
  apresentação oral
