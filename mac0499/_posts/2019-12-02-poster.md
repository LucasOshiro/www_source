---
layout: blog_post
type: mac0499
title: Pôster
---

Pôster do final de MAC0499. Pode ser visto
[aqui]({{site.url}}/{{site.pdf_dir}}/{{page.type}}/poster.pdf):

<embed src="{{site.url}}/{{site.pdf_dir}}/{{page.type}}/poster.pdf" type="application/pdf"
       style="width: 100%; height: 800px">


