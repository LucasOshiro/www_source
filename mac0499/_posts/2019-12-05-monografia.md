---
layout: blog_post
type: mac0499
title: Monografia
---

Monografia de MAC0499. Pode ser vista
[aqui]({{site.url}}/{{site.pdf_dir}}/{{page.type}}/monografia.pdf):

<embed src="{{site.url}}/{{site.pdf_dir}}/{{page.type}}/monografia.pdf" type="application/pdf"
       style="width: 100%; height: 800px">


