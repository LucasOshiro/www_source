---
layout: blog_post
type: mac0499
title: Tema do TCC
---

Análise de Desempenho de Computadores de Baixo Custo em um Sistema de Detecção de Intrusão

Estudo da viabilidade e do desempenho da utilização de computadores de
baixo custo em um sistema de detecção de intrusão com base na análise
de dados gerados por aplicações e sistemas operacionais.


